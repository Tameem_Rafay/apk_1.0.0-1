class DriverEarningRes {
  String message;
  bool success;
  DataMap dataMap;
  DriverEarningRes({this.message, this.success, this.dataMap});
}

class DriverEarningResponse {
  DataMap dataMap;

  DriverEarningResponse({this.dataMap});

  DriverEarningResponse.fromJson(Map<String, dynamic> json) {
    dataMap =
        json['dataMap'] != null ? new DataMap.fromJson(json['dataMap']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataMap != null) {
      data['dataMap'] = this.dataMap.toJson();
    }
    return data;
  }
}

class DataMap {
  List<Page> page;
  List<Earning> earning;

  DataMap({this.page, this.earning});

  DataMap.fromJson(Map<String, dynamic> json) {
    if (json['page'] != null) {
      page = new List<Page>();
      json['page'].forEach((v) {
        page.add(new Page.fromJson(v));
      });
    }
    if (json['earning'] != null) {
      earning = new List<Earning>();
      json['earning'].forEach((v) {
        earning.add(new Earning.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.page != null) {
      data['page'] = this.page.map((v) => v.toJson()).toList();
    }
    if (this.earning != null) {
      data['earning'] = this.earning.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  Null userData;
  int currentPage;
  int pageSize;
  int totalPages;
  int totalElements;
  String nodeName;

  Page(
      {this.userData,
      this.currentPage,
      this.pageSize,
      this.totalPages,
      this.totalElements,
      this.nodeName});

  Page.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    currentPage = json['currentPage'];
    pageSize = json['pageSize'];
    totalPages = json['totalPages'];
    totalElements = json['totalElements'];
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['currentPage'] = this.currentPage;
    data['pageSize'] = this.pageSize;
    data['totalPages'] = this.totalPages;
    data['totalElements'] = this.totalElements;
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class Earning {
  Null userData;
  String createTs;
  List<DriverEarnings> driverEarnings;
  List<CurrentWeakTrips> currentWeakTrips;
  String nodeName;

  Earning(
      {this.userData,
      this.createTs,
      this.driverEarnings,
      this.currentWeakTrips,
      this.nodeName});

  Earning.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    createTs = json['createTs'];
    if (json['driverEarnings'] != null) {
      driverEarnings = new List<DriverEarnings>();
      json['driverEarnings'].forEach((v) {
        driverEarnings.add(new DriverEarnings.fromJson(v));
      });
    }
    if (json['currentWeakTrips'] != null) {
      currentWeakTrips = new List<CurrentWeakTrips>();
      json['currentWeakTrips'].forEach((v) {
        currentWeakTrips.add(new CurrentWeakTrips.fromJson(v));
      });
    }
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['createTs'] = this.createTs;
    if (this.driverEarnings != null) {
      data['driverEarnings'] =
          this.driverEarnings.map((v) => v.toJson()).toList();
    }
    if (this.currentWeakTrips != null) {
      data['currentWeakTrips'] =
          this.currentWeakTrips.map((v) => v.toJson()).toList();
    }
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class DriverEarnings {
  Null userData;
  int id;
  int driverId;
  String driverName;
  String periodStart;
  String periodEnd;
  int totalOrders;
  double totalEarning;
  String paymentDate;
  DriverEarningStatus driverEarningStatus;
  List<DriverEarningTrips> driverEarningTrips;
  String nodeName;

  DriverEarnings(
      {this.userData,
      this.id,
      this.driverId,
      this.driverName,
      this.periodStart,
      this.periodEnd,
      this.totalOrders,
      this.totalEarning,
      this.paymentDate,
      this.driverEarningStatus,
      this.driverEarningTrips,
      this.nodeName});

  DriverEarnings.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    id = json['id'];
    driverId = json['driverId'];
    driverName = json['driverName'];
    periodStart = json['periodStart'];
    periodEnd = json['periodEnd'];
    totalOrders = json['totalOrders'];
    totalEarning = json['totalEarning'];
    paymentDate = json['paymentDate'];
    driverEarningStatus = json['driverEarningStatus'] != null
        ? new DriverEarningStatus.fromJson(json['driverEarningStatus'])
        : null;
    if (json['driverEarningTrips'] != null) {
      driverEarningTrips = new List<DriverEarningTrips>();
      json['driverEarningTrips'].forEach((v) {
        driverEarningTrips.add(new DriverEarningTrips.fromJson(v));
      });
    }
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['id'] = this.id;
    data['driverId'] = this.driverId;
    data['driverName'] = this.driverName;
    data['periodStart'] = this.periodStart;
    data['periodEnd'] = this.periodEnd;
    data['totalOrders'] = this.totalOrders;
    data['totalEarning'] = this.totalEarning;
    data['paymentDate'] = this.paymentDate;
    if (this.driverEarningStatus != null) {
      data['driverEarningStatus'] = this.driverEarningStatus.toJson();
    }
    if (this.driverEarningTrips != null) {
      data['driverEarningTrips'] =
          this.driverEarningTrips.map((v) => v.toJson()).toList();
    }
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class DriverEarningStatus {
  Null userData;
  int id;
  String name;
  String description;
  String nodeName;

  DriverEarningStatus(
      {this.userData, this.id, this.name, this.description, this.nodeName});

  DriverEarningStatus.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    id = json['id'];
    name = json['name'];
    description = json['description'];
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class DriverEarningTrips {
  Null userData;
  int id;
  DriverTrip driverTrip;
  String nodeName;

  DriverEarningTrips({this.userData, this.id, this.driverTrip, this.nodeName});

  DriverEarningTrips.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    id = json['id'];
    driverTrip = json['driverTrip'] != null
        ? new DriverTrip.fromJson(json['driverTrip'])
        : null;
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['id'] = this.id;
    if (this.driverTrip != null) {
      data['driverTrip'] = this.driverTrip.toJson();
    }
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class DriverTrip {
  Null userData;
  String createTs;
  int id;

  String tripCancellationWindow;
  int orderId;
  double deleveryFee;
  double distanceTravelled;
  double cashReceivable;
  String nodeName;

  DriverTrip(
      {this.userData,
      this.createTs,
      this.id,
      this.tripCancellationWindow,
      this.orderId,
      this.deleveryFee,
      this.distanceTravelled,
      this.cashReceivable,
      this.nodeName});

  DriverTrip.fromJson(Map<String, dynamic> json) {
    userData = json['userData'];
    createTs = json['createTs'];
    id = json['id'];

    tripCancellationWindow = json['tripCancellationWindow'];
    orderId = json['orderId'];
    deleveryFee = json['deleveryFee'];
    distanceTravelled = json['distanceTravelled'];
    cashReceivable = json['cashReceivable'];
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userData'] = this.userData;
    data['createTs'] = this.createTs;
    data['id'] = this.id;

    data['tripCancellationWindow'] = this.tripCancellationWindow;
    data['orderId'] = this.orderId;
    data['deleveryFee'] = this.deleveryFee;
    data['distanceTravelled'] = this.distanceTravelled;
    data['cashReceivable'] = this.cashReceivable;
    data['nodeName'] = this.nodeName;
    return data;
  }
}

class CurrentWeakTrips {
  String createTs;
  int id;
  String tripCancellationWindow;
  int orderId;
  double deleveryFee;
  double distanceTravelled;
  double cashReceivable;
  String nodeName;

  CurrentWeakTrips(
      {this.createTs,
      this.id,
      this.tripCancellationWindow,
      this.orderId,
      this.deleveryFee,
      this.distanceTravelled,
      this.cashReceivable,
      this.nodeName});

  CurrentWeakTrips.fromJson(Map<String, dynamic> json) {
    createTs = json['createTs'];
    id = json['id'];
    tripCancellationWindow = json['tripCancellationWindow'];
    orderId = json['orderId'];
    deleveryFee = json['deleveryFee'];
    distanceTravelled = json['distanceTravelled'];
    cashReceivable = json['cashReceivable'];
    nodeName = json['nodeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createTs'] = this.createTs;
    data['id'] = this.id;
    data['tripCancellationWindow'] = this.tripCancellationWindow;
    data['orderId'] = this.orderId;
    data['deleveryFee'] = this.deleveryFee;
    data['distanceTravelled'] = this.distanceTravelled;
    data['cashReceivable'] = this.cashReceivable;
    data['nodeName'] = this.nodeName;
    return data;
  }
}
