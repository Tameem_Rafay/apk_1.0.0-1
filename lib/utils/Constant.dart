class Constant {
/*----------------------- API STATUS CODES ------------------------------------------*/
  static const String SERVICE_RESPONSE_SUCCESS = '001';
  static const String SERVICE_RESPONSE_FAILURE = '002';
  static const String DOES_NOT_EXIST = '003';
  static const String USER_INACTIVE = '004';
  static const String ALREADY_EXIST = '005';
  static const String ILLEGAL_ARGUMENT = '006';

/*-----------------------END API STATUS CODES ------------------------------------------*/

}
