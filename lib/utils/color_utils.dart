import 'package:flutter/material.dart';

class ColorUtils {
  static Color bluePrimary = Color(0xff283440);
  static Color greenPrimary = Color(0xff005F40);
  static Color greyPrimary = Color(0xffC4C4C4);
  static Color smokeWhite = Color(0xffC4C4C4);
  static Color toastColor = Color(0xff005F40);
  static Color redToastColor = Color(0xffFF0000);
  static Color textSmoke = Colors.black45;
  static Color disabledButton = Color.fromARGB(20, 0, 95, 64);
}
