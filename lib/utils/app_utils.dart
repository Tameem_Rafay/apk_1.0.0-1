import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AppUtils {
  static Widget showErrorMessage(message, Function callback) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          message,
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
        GestureDetector(
            onTap: () {
              callback();
            },
            child: Icon(
              Icons.refresh,
              size: 30,
              color: Colors.white,
            )),
      ],
    ));
  }

  static String convertTimeWithMonthName(String date) {
    final DateFormat formatter = DateFormat.yMMMMd(); // e.g. janruary 28, 2021
    final String formatted = formatter.format(DateTime.parse(date));
    return formatted;
  }
}
