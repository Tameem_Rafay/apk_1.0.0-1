import 'package:demoProject/modals/driverEarningModal.dart';
import 'package:demoProject/utils/Constant.dart';
import 'package:demoProject/utils/networkUtils.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DriverEarningService {
  static getDriverEarning() async {
    String url = "https://demo0208117.mockable.io/driver_earning";

    print(url);

    if (await NetworkUtils.checkInternetConnection()) {
      try {
        var response = await http.post(
          url,
          headers: <String, String>{
            'Content-Type': 'application/json',
            'Client-Role': 'DRIVER',
          },
        ).timeout(Duration(seconds: 20));

        // ----------------Switch case -----------
        // We receive the response  code in header and based on header we
        // display the success/error message to user
        // ---------------_____________-----------
        switch (response.headers['api-response-code']) {
          case Constant.SERVICE_RESPONSE_SUCCESS:
            {
              print("------------succes from drier status log ");
              DriverEarningResponse orderListResponse =
                  DriverEarningResponse.fromJson(jsonDecode(response.body));
              return DriverEarningRes(
                message: 'Successful',
                success: true,
                dataMap: orderListResponse.dataMap,
              );
            }
            break;

          case Constant.SERVICE_RESPONSE_FAILURE:
            {
              return DriverEarningRes(
                message: 'Can\t fetch data please retry',
                success: false,
              );
            }
            break;
          case Constant.DOES_NOT_EXIST:
            {
              return DriverEarningRes(
                  message: 'No Record Found', success: false);
            }
            break;

          default:
            {
              return DriverEarningRes(
                  message: 'Sorry for inconvenience please retry later',
                  success: false);
            }
            break;
        }
      } catch (e) {
        print(e.toString());
        return DriverEarningRes(
            message: 'Sorry for inconvenience please retry later!',
            success: false);
      }
    } else {
      return DriverEarningRes(
        message: 'Internet not available',
        success: false,
      );
    }
  }
}
