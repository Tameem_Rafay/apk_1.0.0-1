import 'package:demoProject/modals/driverEarningModal.dart';
import 'package:demoProject/utils/color_utils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:expandable/expandable.dart';
import "package:collection/collection.dart";

class DriverEarningDetail extends StatefulWidget {
  final DriverEarnings driverPayout;
  DriverEarningDetail(this.driverPayout);
  @override
  _DriverEarningDetail createState() => _DriverEarningDetail(driverPayout);
}

class _DriverEarningDetail extends State<DriverEarningDetail> {
  DriverEarnings driverPayout;
  _DriverEarningDetail(this.driverPayout);
  @override
  Widget build(BuildContext context) {
    // ----------------Group By Date -----------
    // Here I group the array elements according to there dates
    // ---------------_______________-----------
    print(driverPayout.driverEarningTrips);
    Map<String, List<DriverEarningTrips>> newMap =
        groupBy(driverPayout.driverEarningTrips.toList(), (obj) {
      String s = obj.driverTrip.createTs
          .substring(0, obj.driverTrip.createTs.indexOf(' '));
      return s;
    });
    print(newMap.entries.toList());

    String startDate = driverPayout.periodStart.split("T")[0];
    String endDate = driverPayout.periodEnd.split("T")[0];
    return Scaffold(
        backgroundColor: ColorUtils.bluePrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(
            "Earnings",
            style: TextStyle(fontSize: 18.0, letterSpacing: -0.3333),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  Text("Total  Earnings",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      )),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Text("Rs. ${driverPayout.totalEarning}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        )),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Text(" ${driverPayout.driverEarningStatus.name}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        )),
                  ),
                  Text("$startDate - $endDate",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      )),
                  ExpandableTheme(
                    data: const ExpandableThemeData(
                      iconColor: Colors.blue,
                      useInkWell: true,
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 10,
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: newMap.entries.toList().length,
                        itemBuilder: (BuildContext context, int index) =>
                            EarningDetail(newMap.entries.toList()[index].value),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class EarningDetail extends StatelessWidget {
  final List<DriverEarningTrips> trips;
  EarningDetail(this.trips);
  @override
  Widget build(BuildContext context) {
    // ----------------Date Formatter  -----------
    // Here I formate the date like 26 August, 2020
    // ---------------_______________-----------
    final DateFormat formatter = DateFormat.yMMMMd();
    final String formatted =
        formatter.format(DateTime.parse(trips.first.driverTrip.createTs));
    // ----------------Expandable Widget -----------
    // Here I use the expandable widget to expande/collapge the widget
    // ---------------_______________-----------
    return ExpandableNotifier(
        initialExpanded: true,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Card(
            color: ColorUtils.bluePrimary,
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: <Widget>[
                ScrollOnExpand(
                  scrollOnExpand: true,
                  scrollOnCollapse: false,
                  child: ExpandablePanel(
                    theme: const ExpandableThemeData(
                      hasIcon: false,
                      iconColor: Colors.white,
                      headerAlignment: ExpandablePanelHeaderAlignment.center,
                      tapBodyToCollapse: true,
                    ),
                    header: Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "$formatted",
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              "${trips.length} orders",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        )),
                    expanded: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: trips.length,
                          itemBuilder: (BuildContext context, int index) =>
                              showEarningPerDay(trips[index]),
                        )
                      ],
                    ),
                    builder: (_, collapsed, expanded) {
                      return Padding(
                        padding:
                            EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        child: Expandable(
                          collapsed: collapsed,
                          expanded: expanded,
                          theme: const ExpandableThemeData(crossFadePoint: 0),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  showEarningPerDay(DriverEarningTrips tripData) {
    return Wrap(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text("Order# ${tripData.driverTrip.orderId}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        )),
                  ),
                ],
              ),
              Text("Rs. ${tripData.driverTrip.deleveryFee} ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  )),
            ],
          ),
        ),
        Divider(color: Colors.white)
      ],
    );
  }
}
