import 'package:collection/collection.dart';
import 'package:demoProject/modals/driverEarningModal.dart';
import 'package:demoProject/sevices/driverEarningService.dart';
import 'package:demoProject/utils/app_utils.dart';
import 'package:demoProject/utils/color_utils.dart';
import 'package:demoProject/utils/networkUtils.dart';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'DriverEarningDetail.dart';

class DriverEarning extends StatefulWidget {
  @override
  _DriverEarningState createState() => _DriverEarningState();
}

class _DriverEarningState extends State<DriverEarning> {
  DriverEarningRes driverResponse;

  driverEarning() async {
    driverResponse = await DriverEarningService.getDriverEarning();
    print(driverResponse.message.toString());
    return driverResponse;
  }

  callback() {
    print("callback is presed");
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorUtils.bluePrimary,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(
            "Earnings",
            style: TextStyle(fontSize: 18.0, letterSpacing: -0.3333),
          ),
        ),
        // ----------------Future Builder -----------
        // Future builder is used to load the data asyncronosly and the
        // display it
        // ---------------_______________-----------
        body: FutureBuilder(
          future: driverEarning(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              driverResponse = snapshot.data;
              if (driverResponse.success != true) {
                return AppUtils.showErrorMessage(
                    driverResponse.message, callback);
              }
            }
            if (snapshot.connectionState == ConnectionState.none ||
                snapshot.hasData == null ||
                snapshot.data == null) {
              return Center(child: NetworkUtils.showLoader());
            }
            // ----------------Group By Date -----------
            // Here I group the array elements according to there dates
            // ---------------_______________-----------
            Map<String, List<CurrentWeakTrips>> currentWeekMap = groupBy(
                driverResponse.dataMap.earning.first.currentWeakTrips.toList(),
                (obj) {
              String s = obj.createTs.substring(0, obj.createTs.indexOf(' '));
              return s;
            });

            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      Text("Current Total  Earnings",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          )),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text("Rs. 1000.00",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Text("Pending",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            )),
                      ),
                      Text("Nov 15, 2020 - Nov 21, 2020",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                          )),
                      ExpandableTheme(
                        data: const ExpandableThemeData(
                          iconColor: Colors.blue,
                          useInkWell: true,
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 10,
                          child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) =>
                                CartOfCurrentWeek(currentWeekMap.entries
                                    .toList()[index]
                                    .value),
                            itemCount: currentWeekMap.entries.toList().length,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 15, 10, 15),
                        child: Wrap(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Previous Weekly Earnings",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                  )),
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, int index) =>
                                  previousEarningCard(driverResponse.dataMap
                                      .earning.first.driverEarnings[index]),
                              itemCount: driverResponse
                                  .dataMap.earning.first.driverEarnings.length,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ));
  }

  previousEarningCard(DriverEarnings driverpayout) {
    String startDate =
        AppUtils.convertTimeWithMonthName(driverpayout.periodStart);
    AppUtils.convertTimeWithMonthName(driverpayout.periodEnd);

    return Card(
      color: ColorUtils.bluePrimary,
      clipBehavior: Clip.antiAlias,
      child: ListTile(
        leading: Text(
          startDate.split("T")[0],
          style: TextStyle(color: Colors.white),
        ),
        trailing: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        DriverEarningDetail(driverpayout)));
          },
          child: Wrap(
            children: [
              Text(
                "Rs. ${driverpayout.totalEarning}",
                style: TextStyle(color: Colors.white),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 5, 5),
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: 13,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CartOfCurrentWeek extends StatelessWidget {
  final List<CurrentWeakTrips> currentWeek;
  CartOfCurrentWeek(this.currentWeek);
  @override
  Widget build(BuildContext context) {
    return ExpandableNotifier(
        initialExpanded: true,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Card(
            color: ColorUtils.bluePrimary,
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: <Widget>[
                ScrollOnExpand(
                  scrollOnExpand: true,
                  scrollOnCollapse: false,
                  child: ExpandablePanel(
                    theme: const ExpandableThemeData(
                      hasIcon: false,
                      iconColor: Colors.white,
                      headerAlignment: ExpandablePanelHeaderAlignment.center,
                      tapBodyToCollapse: true,
                    ),
                    header: Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              currentWeek.first.createTs.split(" ")[0],
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              "${currentWeek.length} orders",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        )),
                    expanded: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: currentWeek.length,
                          itemBuilder: (BuildContext context, int index) =>
                              showEarningPerDay(currentWeek[index]),
                        )
                      ],
                    ),
                    builder: (_, collapsed, expanded) {
                      return Padding(
                        padding:
                            EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        child: Expandable(
                          collapsed: collapsed,
                          expanded: expanded,
                          theme: const ExpandableThemeData(crossFadePoint: 0),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  showEarningPerDay(CurrentWeakTrips tripData) {
    return Wrap(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text("Order# ${tripData.orderId}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        )),
                  ),
                ],
              ),
              Text("Rs. ${tripData.deleveryFee} ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  )),
            ],
          ),
        ),
        Divider(color: Colors.white)
      ],
    );
  }
}
